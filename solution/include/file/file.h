//
// Created by Никита on 14.12.2021.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H

#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_FAILED
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_FAILED
};

enum open_status open_file(FILE **file, const char *path, const char *mode);

enum close_status close_file(FILE *file);

const char* get_file_open_msg(enum open_status status);

const char* get_file_close_msg(enum close_status status);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H
