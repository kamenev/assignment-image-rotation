//
// Created by Никита on 14.12.2021.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "file/file.h"
#include "image/image.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_CONTENT,
    READ_INVALID_SOURCE,
    READ_INVALID_IMAGE,
    READ_FAILED
};

enum write_status {
    WRITE_OK = 0,
    WRITE_CONTENT_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_INVALID_SOURCE
};

struct image get_image_from_bmp(char *file_name);

bool set_image_to_bmp(char *file_name, struct image const *img);


const char* get_read_bmp_msg(enum read_status status);

const char* get_write_bmp_msg(enum write_status status);


#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H

