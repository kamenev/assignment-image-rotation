//
// Created by Никита on 14.12.2021.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>


struct image {
    uint64_t width, height;
    struct pixel *data;
};

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

static const struct image no_image;

struct image image_create(uint64_t width, uint64_t height);

struct pixel get_pixel(const struct image source, size_t x, size_t y);

void image_destroy(struct image image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
