//
// Created by Никита on 14.12.2021.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#include "image/image.h"

struct image rotate(struct image const source);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
