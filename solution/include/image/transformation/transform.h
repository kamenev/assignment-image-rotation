//
// Created by Никита on 29.01.2022.
//
#include "image/image.h"

struct image transform(struct image const source, struct image (transformation)(struct image const));
