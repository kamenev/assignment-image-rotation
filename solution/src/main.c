#include "image/formats/bmp.h"
#include "image/transformation/rotate.h"
#include "image/transformation/transform.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

    if (argc != 3) {
        fprintf(stderr, "Использование: ./image-transformer <исходная картинка> <трансформированная картинка>\n");
        return 1;
    }

    //открываем исходную картинку

    struct image input_image = get_image_from_bmp(argv[1]);

    if(!input_image.data){
        fprintf(stderr, "Ошибка во время формирования структуры из файла");
        image_destroy(input_image);
        return 1;
    }

    //поворачиваем
    struct image transformed_image = transform(input_image, rotate);

    //думаю, право деинициализировать структуру лучше оставить здесь,
    //чем делать это в transform, ведь мы можем использовать структуру еще
    image_destroy(input_image);

    if(!set_image_to_bmp(argv[2], &transformed_image)){
        fprintf(stderr, "Ошибка во время записи в файл из структуры");
        image_destroy(transformed_image);
        return 1;
    }

    image_destroy(transformed_image);

    return 0;
}
