//
// Created by Никита on 14.12.2021.
//
#include "file/file.h"
#include <stdio.h>
#include <stdlib.h>

enum open_status open_file(FILE **file, const char *path, const char *mode) {
    *file = fopen(path, mode);
    if (!*file) {
        return OPEN_FAILED;
    }
    return OPEN_OK;
}

enum close_status close_file(FILE *file) {
    if(fclose(file) == 0){
        return CLOSE_OK;
    }
    return CLOSE_FAILED;
}

static const char* file_open_msg[] = {
        [OPEN_OK] = "Файл успешно открыт",
        [OPEN_FAILED] = "Невозможно открыть файл",
};

static const char* file_close_msg[] = {
        [CLOSE_OK] = "Файл успешно закрыт",
        [CLOSE_FAILED] = "Ошибка закрытия файла",
};

const char* get_file_open_msg(enum open_status status){
    return file_open_msg[status];
}

const char* get_file_close_msg(enum close_status status){
    return file_close_msg[status];
}
