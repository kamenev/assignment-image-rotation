//
// Created by Никита on 14.12.2021.
//
#include "image/transformation/rotate.h"
#include "image/formats/bmp.h"
#include "image/image.h"

struct image rotate(struct image const source) {
    struct image output = image_create((uint64_t) source.height, (uint64_t) source.width);

    if(!output.data || output.width == 0 || output.height == 0){
        image_destroy(output);
        return no_image;
    }

    for (size_t y = 0; y < source.height; y++) {
        for (size_t x = 0; x < source.width; x++) {
            output.data[(source.height - 1 - y) + (output.width * x)] = get_pixel(source, x, y);
        }
    }

    return output;
}
