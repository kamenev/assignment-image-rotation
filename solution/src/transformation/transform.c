//
// Created by Никита on 29.01.2022.
//
#include "image/image.h"


struct image transform(struct image const source, struct image (transformation)(struct image const)){
    struct image output_image = transformation(source);

    if(!output_image.data || output_image.width == 0 || output_image.height == 0){
        fprintf(stderr, "\nОшибка трансформации\n");
        return no_image;
    } else {
        fprintf(stdout, "\nТрансформировано успешно\n");
    }
    return output_image;
}
