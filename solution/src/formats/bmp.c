//
// Created by Никита on 14.12.2021.
//
#include "image/formats/bmp.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

//Значения заголовков BMP
static const uint32_t TYPE = 19778;
static const uint32_t RESERVED = 0;
static const uint32_t HEADER_SIZE = 40;
static const uint16_t PLANES = 1;
static const uint32_t COMPRESSION = 0;
static const uint32_t PIXEL_PER_M = 2835;
static const uint32_t COLORS_USED = 0;
static const uint32_t COLORS_IMPORTANT = 0;
static const size_t BIT_COUNT = 24;


#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static size_t get_padding(size_t width);

static enum read_status read_bmp_header(FILE *file, struct bmp_header *header);

static enum read_status read_bmp_content(FILE *file, struct image *img);

static size_t get_image_size(struct image const *img);

static struct bmp_header create_bmp_header(struct image const *img);

static enum write_status write_bmp_header(FILE *file, struct image const *img);

static enum write_status write_bmp_content(FILE *file, struct image const *img);

static enum read_status from_bmp(FILE *file, struct image *img);

static enum write_status to_bmp(FILE *file, struct image const *img);

static const char* read_bmp_msg[] = {
        [READ_OK] = "BMP файл успешно прочитан",
        [READ_INVALID_SIGNATURE] = "Входной BMP файл с неверной сигнатурой",
        [READ_INVALID_BITS] = "Входной BMP файл имеет некорректную битность",
        [READ_INVALID_HEADER] = "Ошибка чтения заголовкой входного BMP файла",
        [READ_INVALID_CONTENT] = "Ошибка чтения содержимого BMP файла",
        [READ_INVALID_SOURCE] = "Недействительный файл для чтения",
        [READ_INVALID_IMAGE] = "Структура изображения сформировалась некорректно",
        [READ_FAILED] = "Ошибка во время чтения"
};

static const char* write_bmp_msg[] = {
        [WRITE_OK] = "BMP файл успешно записан",
        [WRITE_CONTENT_ERROR] = "Ошибка записи содержимого в BMP файл",
        [WRITE_HEADER_ERROR] = "Ошибка записи заголовков в BMP файл",
        [WRITE_INVALID_SOURCE] = "Недействительный файл для записи"
};


struct image get_image_from_bmp(char *file_name){

    FILE *input_file;

    const enum open_status open_input_file = open_file(&input_file, file_name, "rb");

    if (open_input_file != 0) {
        fprintf(stderr, "%s: %s\n", get_file_open_msg(open_input_file), file_name);
        return no_image;
    } else {
        fprintf(stdout, "%s: %s\n", get_file_open_msg(open_input_file), file_name);
    }

    //конвертируем bmp в структуру image
    struct image input_image = {0};

    const enum read_status read_bmp_status = from_bmp(input_file, &input_image);

    //Закрываем файл после прочтения
    const enum close_status close_input_file = close_file(input_file);
    if(close_input_file != 0){
        fprintf(stderr, "%s: %s\n", get_file_close_msg(close_input_file), file_name);
        return no_image;
    } else {
        fprintf(stdout, "%s: %s\n", get_file_close_msg(close_input_file), file_name);
    }

    if (read_bmp_status != 0) {
        fprintf(stderr, "%s: %s\n", get_read_bmp_msg(read_bmp_status), file_name);
        return no_image;
    } else {
        fprintf(stdout, "%s: %s\n", get_read_bmp_msg(read_bmp_status), file_name);
    }
    return input_image;
}

bool set_image_to_bmp(char *file_name, struct image const *source){
    //открываем файл для записи перевернутой картинки
    FILE *output_file;

    const enum open_status open_output_file = open_file(&output_file, file_name, "wb");

    if (open_output_file != 0) {
        fprintf(stderr, "%s: %s\n", get_file_open_msg(open_output_file), file_name);
        return false;
    } else {
        fprintf(stdout, "%s: %s\n", get_file_open_msg(open_output_file), file_name);
    }

    //конвертируем структуру image в bmp файл

    const enum write_status write_bmp_status = to_bmp(output_file, source);

    const enum close_status close_output_file = close_file(output_file);
    if(close_output_file != 0){
        fprintf(stderr, "%s: %s\n", get_file_close_msg(close_output_file), file_name);
        return false;
    } else {
        fprintf(stdout, "%s: %s\n", get_file_close_msg(close_output_file), file_name);
    }

    if (write_bmp_status != 0) {
        fprintf(stderr, "%s: %s\n", get_write_bmp_msg(write_bmp_status), file_name);
        return false;
    } else {
        fprintf(stdout, "%s: %s\n", get_write_bmp_msg(write_bmp_status), file_name);
    }
    return true;
}






static enum read_status from_bmp(FILE *file, struct image *img) {

    if (!file) {
        return READ_INVALID_SOURCE;
    }

    //Считываем заголовок
    struct bmp_header header = {0};
    enum read_status header_read_status = read_bmp_header(file, &header);
    if (header_read_status != READ_OK) {
        return header_read_status;
    }

    //Создаем структуру с известными параметрами
    *img = image_create((uint64_t) header.biWidth, (uint64_t) header.biHeight);
    if(!img->data || img->width == 0 || img->height == 0){
        image_destroy(*img);
        return READ_INVALID_IMAGE;
    }

    //Считываем изображение
    enum read_status content_read_status = read_bmp_content(file, img);
    if (content_read_status != READ_OK) {
        image_destroy(*img);
        return content_read_status;
    }

    return READ_OK;

}

static enum write_status to_bmp(FILE *file, struct image const *img) {

    if (!file) {
        return WRITE_INVALID_SOURCE;
    }

    //Аналогично записываем заголовок и изображение

    enum write_status header_write_status = write_bmp_header(file, img);
    if (header_write_status != WRITE_OK) {
        return header_write_status;
    }
    enum write_status content_write_status = write_bmp_content(file, img);
    if (content_write_status != WRITE_OK) {
        return content_write_status;
    }

    return WRITE_OK;

}

static enum read_status read_bmp_header(FILE *file, struct bmp_header *header) {

    if (fread(header, sizeof(struct bmp_header), 1, file) != 1) {
        return READ_INVALID_HEADER;
    };

    if (header->biBitCount != BIT_COUNT) {
        return READ_INVALID_BITS;
    }

    if (header->bfType != TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    return READ_OK;
}


static enum read_status read_bmp_content(FILE *file, struct image *img) {

    for (size_t i = 0; i < img->height; i++) {
        //Считываем строку пикселей целиком и записываем в структуру image, проверям успешность считывания
        if (fread((img->data + i * img->width), sizeof(struct pixel) * (size_t)(img->width), 1, file) != 1) {
            return READ_INVALID_CONTENT;
        }
        //Пропускаем padding байты
        if (fseek(file, get_padding(img->width), SEEK_CUR) != 0) {
            return READ_INVALID_CONTENT;
        };
    }
    return READ_OK;

}

static enum write_status write_bmp_header(FILE *file, struct image const *img) {
    //Создаем и заполняем структуру заголовками
    struct bmp_header new_bmp_header = create_bmp_header(img);

    //Записываем заголовок в файл
    if (fwrite(&new_bmp_header, sizeof(struct bmp_header), 1, file) != 1) {
        return WRITE_HEADER_ERROR;
    };

    //Смещаемся к точке начала записи контента
    if (fseek(file, sizeof(struct bmp_header), SEEK_SET) != 0) {
        return WRITE_HEADER_ERROR;
    };

    return WRITE_OK;
}

static struct bmp_header create_bmp_header(struct image const *img) {
    return (struct bmp_header) {
            .bfType = TYPE,
            .biSize = HEADER_SIZE,
            .bfReserved = RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION,
            .biSizeImage = get_image_size(img),
            .bfileSize = get_image_size(img) + sizeof(struct bmp_header),
            .biXPelsPerMeter = PIXEL_PER_M,
            .biYPelsPerMeter = PIXEL_PER_M,
            .biClrUsed = COLORS_USED,
            .biClrImportant = COLORS_IMPORTANT,
    };
}

static size_t get_image_size(struct image const *img){
    return ((img->width + get_padding(img->width)) * img->height) * sizeof(struct pixel);
}

static enum write_status write_bmp_content(FILE *file, struct image const *img) {
    char padding[4] = {0};
    for (size_t i = 0; i < img->height; i++) {
        //Записываем строку пикселей целиком и проверяем успешность записи
        if (fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, file) != 1) {
            return WRITE_CONTENT_ERROR;
        }
        //Записываем padding байты
        if (fwrite(padding, sizeof(uint8_t) * get_padding(img->width), 1, file) != 1) {
            return WRITE_CONTENT_ERROR;
        }
    }
    return WRITE_OK;
}

//Вспомогательная функция для расчета padding
static size_t get_padding(size_t width) {
    return (4 - width * sizeof(struct pixel) % 4) % 4;
}

const char* get_read_bmp_msg(enum read_status status){
    return read_bmp_msg[status];
}

const char* get_write_bmp_msg(enum write_status status){
    return write_bmp_msg[status];
}
