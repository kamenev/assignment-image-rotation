//
// Created by Никита on 14.12.2021.
//
#include "image/image.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdlib.h>


struct image image_create(uint64_t width, uint64_t height) {
    struct pixel* data = malloc(sizeof(struct pixel) * width * height);
    if(data) {
        return (struct image) {
                .data = data,
                .height = height,
                .width = width
        };
    }
    return no_image;
}

static const struct image no_image = (struct image){.data = NULL, .width = 0, .height = 0};

struct pixel get_pixel(const struct image source, size_t x, size_t y){
    return source.data[source.width * y + x];
}

void image_destroy(struct image image) {
    if(image.data) {
        free(image.data);
    }
    image.data = NULL;
    image.height = 0;
    image.width = 0;
}
